"use strict";

/**
 * This file should be used to configure and return the aws object.
 * Sense that we are in the development environment and return the configured object.
 * If the development NODE_ENV is not set, return an object that throws an error.
 *
 * Scratch:
 * // const Kraken = require("kraken");
 */

/**
 * http://docs.aws.amazon.com/AWSJavaScriptSDK/guide/node-configuring.html
 */

/**
 * We don't need to do shit for development environment!
 * AWS SDK takes care of the configuration for the current user.
 * That's fucking cool.
 *
 * Sean's keys look like they still work though. So that's nice for me. Secret, but fun.
 * aws.config.update({accessKeyId:'AKIAIAJYRVVJFUGWFYWQ',secretAccessKey:'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',region:"us-east-1"});
 */

function EnvironmentError(message) {
	this.name = "EnvironmentError";
	this.message = (message || "Unspecified error message.");
}
EnvironmentError.prototype = Error.prototype;

let env = process.env.NODE_ENV;
let useCredFile = process.env.USE_CREDENTIAL_FILE;
if (!env || env !== 'development') {
	console.log("Only development environment is supported at this time.");
	process.exit(0);
}

let credentialFile = "/Users/nygaard/.aws/credentials";
let readline = require('readline');
let fs = require('fs');

let aws = require("aws-sdk");
// process.env.AWS_PROFILE = "symphony-compose";
/**
 * Chris' hard keys.
 */
let key = "AKIAJMDP7NY57B7VHK3Q";
let secret = "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";
let region_hard = "us-east-1";
/**
 * Sean's keys. Might as well use these.
 */
	// "key": "AKIAIAJYRVVJFUGWFYWQ",
	// "secret": "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",

let s3;
// new aws.S3();
if(useCredFile){
	s3 = new aws.S3();
}else{
	aws.config.update({
		accessKeyId: 'AKIAJMDP7NY57B7VHK3Q',
		secretAccessKey: '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',
		region: "us-east-1"
	});
	s3 = new aws.S3();
}

function getAwsCredentials() {
	if (useCredFile) {
		// s3 = new aws.S3();
		return new Promise((resolve)=> {
			let line = readline.createInterface({
				input: fs.createReadStream(credentialFile)
			});
			let aws_access_key_id = null;
			let aws_secret_access_key = null;
			let region = null;

			line.on('line', (line) => {
				let tokens = line.split(" ");
				switch (tokens[0]) {
					case "aws_access_key_id":
						aws_access_key_id = tokens.slice()[2];
						break;
					case "aws_secret_access_key":
						aws_secret_access_key = tokens.slice()[2];
						break;
					case "region":
						region = tokens.slice()[2];
						break;
					default:
						break;
				}
			});

			line.on("close", ()=> {
				resolve({
					aws_access_key_id: aws_access_key_id,
					region: region,
					aws_secret_access_key: aws_secret_access_key
				});
			});

		})
	} else {


		// s3 = new aws.S3();
		return new Promise((resolve)=> {
			// let aws_access_key_id = key;
			// let aws_secret_access_key = secret;
			// let region = region_hard;
			resolve({
				aws_access_key_id: key,
				region: region_hard,
				aws_secret_access_key: secret
			});
		})
	}
}

export {
	s3,
	getAwsCredentials
}