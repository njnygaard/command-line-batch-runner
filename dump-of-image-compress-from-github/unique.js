/*
	Make a dict
	go through list of alls in a folder
	get the file hash
	check the dict see if it's in or not
	if it's in, skip. if not in, put the unique name as key and full filename as value
	after going through the whole list, go through the dict and move all the files in the dict into a different folder
*/

'use strict';

const fs = require('fs');
const md5 = require('md5');
const path = require('path');
const _ = require('lodash');

const uniqueFolder = 'unique';

function buildDict(dir, callback) {
	let dict = {};
	fs.readdir(dir, (err, files) => {
		if (err) { console.log('error:', err); return }
		for (let file of files) {
			let filePath = path.join(dir, file);
			if (!fs.statSync(filePath).isDirectory()) {
				console.log('reading file ' + file);
				let data = fs.readFileSync(filePath);
				let hash = md5(data);
				if (!dict.hasOwnProperty(hash)) {
					dict[hash] = file;
				}
				console.log('done');				
			}
		}
		console.log('--------------- done building dict ---------------');
		callback && callback(dict, dir);
	});
}

function moveFiles(dict, dir) {
	_.map(dict, file => {
		fs.renameSync(path.join(dir, file), path.join(dir, uniqueFolder, file));
	});
	console.log('--------------- done with coping ---------------');
}


function moveSingle(dir) {
	fs.readdir(dir, (err, files) => {
		if (err) { console.log('error:', err); return }
		let previous;
		_.each(files, file => {
			
			fs.renameSync(path.join(dir, file), path.join(dir, uniqueFolder, file));
		});
		
	});
}


// buildDict('/Users/shawiz/dev/symphony/data/2images/neffheadwear', moveFiles);
