/*
For list of bad images
get a set of hashes that are bad
go through all images, and move all images that are bad to remove folder

In s3, delete these images
*/

'use strict';

const fs = require('fs');
const md5 = require('md5');
const path = require('path');
const _ = require('lodash');

const badFolder = 'bad';
const removeFolder = 'remove';

/*
We know these are unique
*/
function buildSet(dir, callback) {
	let set = {};
	fs.readdir(dir + badFolder, (err, files) => {
		if (err) { console.log('error:', err); return }
		for (let file of files) {
			console.log('reading file ' + file);
			let data = fs.readFileSync(path.join(dir, badFolder, file));
			let hash = md5(data);
			set[hash] = file;
		}
		console.log('--------------- done building set ---------------');
		callback && callback(set, dir);
	});
}

function moveBad(set, dir) {
	fs.readdir(dir, (err, files) => {
		if (err) { console.log('error:', err); return }
		_.map(files, file => {
			let filePath = path.join(dir, file);
			if (!fs.statSync(filePath).isDirectory()) {
				let data = fs.readFileSync(filePath);
				let hash = md5(data);
				if (set.hasOwnProperty(hash)) {
					fs.renameSync(filePath, path.join(dir, removeFolder, file));
				}
			}
		});
	});
}

buildSet('/Users/shawiz/dev/symphony/data/2images/bollandbranch/', moveBad);