import {s3} from "./register-aws";
/*

 Image compressor with S3 and TinyPNG
 Steps:
 1. Get into a bucket, find pngs files that are > 20kb in batches
 2. for each brand:
 1. For each file, copy it to another backup bucket with same key
 2. For each file, compress the file, and copy it to a backup bucket with the same key

 // Have a script that copy file back to the same folder so you can recover
 // Keep track of files updated and size saved

 release environment uses the same image file as the production.

 */

'use strict';

const fs = require('fs');
const path = require('path');
// const aws = require('aws-sdk');
const _ = require('lodash');
const tinify = require("tinify");
tinify.key = 'pD3SDGBgg545y3IQOaIvRNWFA9YaI6kM';

// const awsKey = 'AKIAIAJYRVVJFUGWFYWQ';
// const awsSecret = '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';
// const s3Region = 'us-east-1';
// const s3 = new aws.S3();

const sourceBucket = 'symphony-sites';
const backupBucket = 'symphony-private';
const imagesFolder = 'images/sites/';

// image format, can support .jpg
const fileFormat = '.jpg';
// file suffix for optimized image
const fileSuffix = '-optimized';
// image compression threshold in KB. won't compress files under this size
const minFileSize = '50';
// default file permission for s3
const defaultFilePermission = 'public-read';

let allImages = [];

/**
 * Unused from Sean
 */
// function fileFilter(file) {
// 	return file.Key.indexOf(fileFormat) != -1 && file.Size >= minFileSize * 1024;
// }


/**
 * Unused from Sean
 */
// function listBrandImages(brand, callback) {
// 	let marker = imagesFolder + brand;
// 	listImages(sourceBucket, marker, marker, fileFilter, () => {
// 		let keys = _.map(allImages, 'Key');
// 		console.log(keys);
// 		console.log('compressing ' + keys.length + ' images');
// 		callback && callback(keys);
// 	});
// }
/**
 * Unused from Sean
 */
// function copyToBackup(key, callback) {
// 	let params = {
// 		Bucket: backupBucket,
// 		CopySource: sourceBucket + '/' + key,
// 		Key: key,
// 		ACL: defaultFilePermission
// 	};
// 	s3.copyObject(params, (err, data) => {
// 		if (err) { console.log('error:', err); }
// 	//	console.log('copied');
// 		callback && callback();
// 	});
// }

/**
 * Unused from Sean
 */
// function compress(key, callback) {
// 	let remoteKey = backupBucket + '/' + key.replace(fileFormat, fileSuffix + fileFormat);
// 	s3.getObject({Bucket: sourceBucket, Key: key}, (err, data) => {
// 		if (err) { console.log('error:', err); }
// 		var source = tinify.fromBuffer(data.Body);
// 		source.store({
// 			service: 's3',
// 			aws_access_key_id: awsKey,
// 			aws_secret_access_key: awsSecret,
// 			region: s3Region,
// 			path: remoteKey
// 		}).location(() => {
// 			console.log('upladed ' + key + ' to s3');
// //			callback && callback();
// 		});
// 		callback && callback();
// 	});
// }

/**
 * Unused from Sean
 * @param brand
 * @param callback
 */
// async loop
// function run(brand) {
// 	listBrandImages(brand, keys => {
// 		function compressAndBackup(i) {
// 			if (i < keys.length) {
// 				console.log('start processing file ' + i + ': ' + keys[i] + '...');
// 				copyToBackup(keys[i]);
// 				compress(keys[i], () => {
// 					console.log('done with file ' + i);
// 					compressAndBackup(i + 1);
// 				});
// 			} else {
// 				console.log('job complete');
// 			}
// 		}
// 		compressAndBackup(0);
// 	});
// }



// function copyToOriginal(key, callback) {
// 	let destKey = key.replace(fileSuffix, '');
// 	let params = {
// 		Bucket: sourceBucket,
// 		CopySource: backupBucket + '/' + key,
// 		Key: destKey,
//         // Expires:"",
// 		ACL: defaultFilePermission
// 	};
// 	s3.copyObject(params, (err, data) => {
// 		if (err) { console.log('error:', err); }
// 		console.log('copied');
// 		callback && callback();
// 	});
// }
function listImages(bucket, prefix, marker, filter, callback) {
    s3.listObjects({Bucket: bucket, Marker: marker}, (err, data) => {
        if (err) { console.log('error:', err); return }

        let files = data.Contents;
        let nextMarker = files[files.length - 1].Key;

        // let images = _.filter(files, file => file.Key.indexOf(prefix) != -1 && filter(file));
        let images = _.filter(files, (file) => {
            file.Key.indexOf(prefix) != -1 && filter(file)
        });
        allImages = allImages.concat(images);

        if (data.IsTruncated && nextMarker.indexOf(prefix) != -1) {
            listImages(bucket, prefix, nextMarker, filter, callback);
        } else {
            callback && callback();
        }
    });
}

function listCompressedImages(brand, callback) {
    let marker = imagesFolder + brand;
    listImages(
        backupBucket,
        marker,
        marker,
        file => file.Key.indexOf(fileSuffix) != -1,
        () => {
            let keys = _.map(allImages, 'Key');
            console.log(keys);
            console.log('replacing ' + keys.length + ' images');
            callback && callback(keys);
        }
    );
}

function copyToTest(key, callback) {
    let destKey = key.replace(fileSuffix, '');
    let params = {
        Bucket: "symphony-private",
        CopySource: backupBucket + '/' + key,
        Key: destKey,
        // Expires:"",
        ACL: defaultFilePermission
    };
    s3.copyObject(params, (err, data) => {
        if (err) { console.log('error:', err); }
        console.log('copied');
        callback && callback();
    });
}

function runReplace(brand) {
    listCompressedImages(brand, keys => {
        console.log(keys);
        function replaceFile(i) {
            if (i < keys.length) {
                console.log('start replacing file ' + i + ': ' + keys[i] + '...');
                // copyToOriginal(keys[i], () => {
                copyToTest(keys[i], () => {
                    console.log('done with file ' + i);
                    replaceFile(i + 1);
                });
            } else {
                console.log('job complete');
            }
        }
        replaceFile(0);
    });
}

/**
 * Unused from Sean
 * @param bucket
 * @param brand
 * @param dir
 */
// function removeFromS3(bucket, brand, dir) {
// 	fs.readdir(dir, (err, files) => {
// 		if (err) { console.log('error:', err); return }
// 		let set = _.zipObject(files, files);
// 		listCompressedImages(brand, keys => {
// 			function removeFile(i) {
// 				if (i < keys.length) {
// 					let filePaths = keys[i].split('/');
// 					let fileName = filePaths[filePaths.length - 1];
// 					console.log(fileName);
//
// 					if (set.hasOwnProperty(fileName)) {
// 						console.log('start removing file ' + i + ': ' + fileName + '...');
// 						s3.deleteObject({Bucket: bucket, Key: keys[i]}, (err, data) => {
// 							if (err) { console.log('error:', err); }
// 							console.log(data);
// 							removeFile(i + 1);
// 						});
// 					} else {
// 						removeFile(i + 1);
// 					}
// 				} else {
// 					console.log('job complete');
// 				}
// 			}
// 			removeFile(0);
// 		});
// 	});
// }

//run('stapler');
runReplace('stapler');
//removeFromS3('www.sneakpeeq.com', 'bumbleride', '/Users/shawiz/dev/symphony/data/2images/bumbleride/remove');