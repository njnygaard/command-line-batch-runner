import * as aws from "./register-aws";
import {kraken} from "./register-kraken";


const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');


'use strict';
const errorLog = "/Users/nygaard/.aws/credentials";
const readline = require('readline');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const tinify = require("tinify");
tinify.key = 'pD3SDGBgg545y3IQOaIvRNWFA9YaI6kM';

const s3 = aws.s3;

const sourceBucket = 'symphony-sites';
const backupBucket = 'symphony-private';
const compressBucket = 'symphony-private';
const imagesFolder = 'images/sites/';
const krakenStorageFolder = 'krakenio/';
const backupStorageFolder = 'krakenio/backup/';

// image format, can support .jpg
const fileFormat = '.jpg';
// file suffix for optimized image
// const fileSuffix = '-optimized';
const fileSuffix = ".optimized.jpg";
// image compression threshold in KB. won't compress files under this size
const minFileSize = '5';
// default file permission for s3
const defaultFilePermission = 'public-read';

let allImages = [];

function fileFilter(file) {
	return file.Key.indexOf(fileFormat) != -1 && file.Size >= minFileSize * 1024;
}

function listImages(bucket, prefix, marker, filter, callback) {
	s3.listObjects({Bucket: bucket, Marker: marker}, (err, data) => {
		if (err) {
			console.log('listObjects error:', err, marker);
			return
		}

		let files = data.Contents;
		let nextMarker = files[files.length - 1].Key;

		let images = _.filter(files, (value) => {
			return (value.Key.indexOf(prefix) !== -1 && filter(value));
		});

		allImages = allImages.concat(images);
		console.log("Adding " + images.length + " images to list of " + allImages.length + " images.");

		if (data.IsTruncated && nextMarker.indexOf(prefix) != -1) {
			listImages(bucket, prefix, nextMarker, filter, callback);
		} else {
			callback && callback();
		}
	});
}

function listBrandImages(brand, callback) {
	let marker = imagesFolder + brand;
	listImages(sourceBucket, marker, marker, fileFilter, () => {
		let keys = _.map(allImages, 'Key');
		console.log('compressing ' + keys.length + ' images');
		callback && callback(keys);
	});
}

function listCompressedImages(brand, callback) {
	let marker = krakenStorageFolder + imagesFolder + brand;
	listImages(
		compressBucket,
		marker,
		marker,
		(file) => {
			return (file.Key.indexOf(fileSuffix) !== -1);
		},
		() => {
			let keys = _.map(allImages, 'Key');
			console.log('replacing ' + keys.length + ' images');
			callback && callback(keys);
		}
	);
}

/**
 * ported
 * @param key
 * @param callback
 */
function copyToBackup(key, callback) {
	/**
	 * TODO: There is an error if the filename contains '+'
	 * @type {{Bucket: string, CopySource: string, Key: string, ACL: string}}
	 */
		// if (key.indexOf('1453331389314_C-Free+Website+Key+Photo+Image.jpg') > -1) {
		// 	console.log('found the thing 1453331389314_C-Free+Website+Key+Photo+Image.jpg');
		// }
	let params = {
			Bucket: backupBucket,
			CopySource: sourceBucket + '/' + key,
			Key: backupStorageFolder + key,
			ACL: defaultFilePermission
		};
	s3.copyObject(params, (err) => {
		if (err) {
			callback && callback('fail', key);
		} else {
			callback && callback('success', key);
		}
	});
}

function compress(callback, key, index, collection) {
	aws.getAwsCredentials().then((creds)=> {
		copyToBackup(key, (status, err)=> {
			console.log("copy to backup", key, status, err || '');
		});
		/**
		 * TODO: Move the credentials to a service.
		 */
		let date = new Date();
		date.setFullYear(date.getFullYear() + 50);


		/**
		 * TODO: This is the stuff for the file downloading compression version of the workflow.
		 */
		let params = {
			Bucket: sourceBucket,
			Key: key
		};


		let directoryName = key.split('/')[2];
		let fileName = key.split('/')[3];

		ensureExists(__dirname + '/tempImages/' + directoryName, '0777', function (err) {
			if (err) {
				console.log('error creating directory');
			}
			else {
				let tempImageFile = fs.createWriteStream(__dirname + '/tempImages/' + directoryName + '/' + fileName);
				tempImageFile.on('open', function (fd) {
					s3.getObject(params)
						.on('httpData', function (chunk) {
							tempImageFile.write(chunk);
						})
						.on('httpDone', function () {
							tempImageFile.end();
							console.log('storage done for', tempImageFile.path);

							imagemin([__dirname + '/tempImages/' + directoryName + '/' + fileName], 'compress', {
								plugins: [
									imageminMozjpeg({
										quality: 60,
									}),
									imageminPngquant({
										quality: '55-65',
									})
								]
							}).then((file) => {


								let params = {
									Bucket: compressBucket,
									Key: krakenStorageFolder + key + fileSuffix,
									Body: file[0].data
								};


								s3.putObject(params, function (err, data) {


									if (err) {
										console.log('Fail. Putting image on s3.', err, err.stack);
									}
									else {
										console.log('Success. Putting image on s3.', data);
									}

									if (index === collection.length - 1) {
										callback && callback()
									}

								});

							});


						})
						.send();
				});
			}
		});

		function ensureExists(path, mask, cb) {
			if (typeof mask == 'function') { // allow the `mask` parameter to be optional
				cb = mask;
				mask = '0777';
			}
			fs.mkdir(path, mask, function (err) {
				if (err) {
					if (err.code == 'EEXIST') cb(null); // ignore the error if the folder already exists
					else cb(err); // something else went wrong
				} else cb(null); // successfully created folder
			});
		}


		/**
		 * TODO: This is the Kraken stuff.
		 * I should abstract the details of the compression at this point.
		 */
		// let krakenParameters = {
		// 	"s3_store": {
		// 		key: creds.aws_access_key_id,
		// 		secret: creds.aws_secret_access_key,
		// 		region: creds.region,
		// 		bucket: compressBucket,
		// 		path: krakenStorageFolder + key + fileSuffix,
		// 		headers: {
		// 			"Cache-Control": "max-age=2592000000",
		// 			"Expires": date
		// 		}
		// 	},
		// 	url: "https://s3.amazonaws.com/" + sourceBucket + "/" + key,
		// 	wait: true,
		// 	lossy: true
		// };
		//
		// kraken.url(krakenParameters, (status)=> {
		// 	if (status.success) {
		// 		console.log("Success. Optimized image URL: %s", status.kraked_url);
		// 	} else {
		// 		if (status.message.indexOf("EMFILE") > -1) {
		// 			console.log("found an EMFILE")
		// 		}
		// 		console.log("Fail. Error message: %s", status.message, key);
		//
		// 	}
		// 	if (index === collection.length - 1) {
		// 		callback && callback()
		// 	}
		//
		// })


	});
}


function copyToOriginal(callback, key, index, collection) {
	let destKey = key.replace(fileSuffix, "").replace(krakenStorageFolder, "");
	let params = {
		Bucket: sourceBucket,
		CopySource: backupBucket + '/' + key,
		Key: destKey,
		ACL: defaultFilePermission
	};
	s3.copyObject(params, (err) => {
		if (err) {
			console.log("error copying to original:", key);
		} else {
			console.log("success copying to original:", key)
		}
		if (index === collection.length - 1) {
			callback && callback()
		}
	});
}

function restoreFromBackup(key) {
	let params = {
		Bucket: sourceBucket,
		CopySource: backupBucket + '/' + backupStorageFolder + key,
		Key: key,
		ACL: defaultFilePermission
	};
	s3.copyObject(params, (err) => {
		if (err) {
			console.log("error restoring file:", key);
		} else {
			console.log("successfully restored:", key);
		}
	});
}

function run(brand) {
	listBrandImages(brand, (keys)=> {
			// keys.forEach(compress);
			batch(compress, keys, 100);
		}
	);
}

function batch(fn, array, size) {

	let remainder = array.slice(size);
	if (remainder.length) {
		array.slice(0, size).forEach(fn.bind(null, ()=> {
			console.log("firing callback given to function", remainder.length);
			batch(fn, remainder, size);
		}));
	} else {
		array.forEach(fn.bind(null, null));
		return true;
	}
}

function replace(brand) {
	listCompressedImages(brand, (keys)=> {
		// keys.forEach(copyToOriginal);
		batch(copyToOriginal, keys, 100);
	})
}

function restore(brand) {
	listBrandImages(brand, (keys)=> {
		keys.forEach(restoreFromBackup);
	})
}

function logInputs(value, index, collection) {
	console.log("Logging:", value);
}

// restore('stapler');

// run('petermillar');
// replace('petermillar');
// run('stapler');
// replace('stapler');
// run('kiwitea');
// replace('kiwitea');
// run('petermillar');

let args = process.argv;

if (args.length <= 2) {
	console.log("Please provide some arguments.");
	process.exit(0);
} else if (args.length <= 3) {
	console.log("Please provide a brand name.");
	process.exit(0);
} else {
	let command = args[2];
	let brandName = args[3];
	switch (command) {
		case "run":
			run(brandName);
			break;
		case "replace":
			replace(brandName);
			break;
		case "restore":
			restore(brandName);
			break;
		default:
			break;
	}
}